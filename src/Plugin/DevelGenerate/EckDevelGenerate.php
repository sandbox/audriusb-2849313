<?php

namespace Drupal\devel_generate_eck\Plugin\DevelGenerate;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\devel_generate\DevelGenerateBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\eck\Entity\EckEntity;
use Drupal\eck\Entity\EckEntityType;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Provides a ExampleDevelGenerate plugin.
 *
 * @DevelGenerate(
 *   id = "devel_generate_eck",
 *   label = @Translation("Eck"),
 *   description = @Translation("Generate a given number of eck elements. Optionally delete current eck elements."),
 *   url = "eck",
 *   permission = "administer devel_generate",
 *   settings = {
 *     "num" = 50,
 *     "kill" = FALSE,
 *     "title_length" = 4
 *   }
 * )
 */
class EckDevelGenerate extends DevelGenerateBase implements ContainerFactoryPluginInterface {

  protected $eckTypeStorage;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager) {
    $this->eckTypeStorage = EckEntityType::loadMultiple();
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {;
    foreach ($this->eckTypeStorage as $type) {
      $header = [
        'type' => $type->label(),
      ];

      foreach ($this->getBundles($type) as $bundle) {
        $options[$bundle->id()] = array(
          'type' => array('#markup' => $bundle->label()),
        );
      }

      $form[$type->id()] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $options,
      );
    }

    $form['kill'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('<strong>Delete all content</strong> in these entity types before generating new content.'),
      '#default_value' => $this->getSetting('kill'),
    );

    $form['num'] = array(
      '#type' => 'number',
      '#title' => $this->t('How many entities would you like to generate?'),
      '#default_value' => $this->getSetting('num'),
      '#required' => TRUE,
      '#min' => 0,
    );

    $form['title_length'] = array(
      '#type' => 'number',
      '#title' => $this->t('Maximum number of words in titles'),
      '#default_value' => $this->getSetting('title_length'),
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 255,
    );

    $options = array();
    // We always need a language.
    $languages = $this->languageManager->getLanguages(LanguageInterface::STATE_ALL);
    foreach ($languages as $langcode => $language) {
      $options[$langcode] = $language->getName();
    }

    $form['add_language'] = array(
      '#type' => 'select',
      '#title' => $this->t('Set language on entities'),
      '#multiple' => TRUE,
      '#description' => $this->t('Requires locale.module'),
      '#options' => $options,
      '#default_value' => array(
        $this->languageManager->getDefaultLanguage()->getId(),
      ),
    );

    $form['#redirect'] = FALSE;
    return $form;
  }

  /**
   * Determine language based on $results.
   */
  protected function getLangcode($results) {
    if (isset($results['add_language'])) {
      $langcodes = $results['add_language'];
      $langcode = $langcodes[array_rand($langcodes)];
    }
    else {
      $langcode = $this->languageManager->getDefaultLanguage()->getId();
    }
    return $langcode;
  }

  private function getBundles($type) {
    $entity_manager = \Drupal::entityTypeManager();
    $bundleStorage = $entity_manager->getStorage($type->id() . '_type');
    return $bundleStorage->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  function settingsFormValidate(array $form, FormStateInterface $form_state) {
    $found_selected = FALSE;
    
    foreach ($this->eckTypeStorage as $type) {
      if (array_filter($form_state->getValue($type->id()))) {
        $found_selected = TRUE;
        break;
      }
    }

    if (!$found_selected) {
      $form_state->setErrorByName('', $this->t('Please select at least one entity type'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateDrushParams($args) {
    
  }

  /**
   * {@inheritdoc}
   */
  protected function generateElements(array $values) {
    if ($values['num'] <= 50) {
      $this->generateContent($values);
    }
    else {
      $this->generateBatchContent($values);
    }
  }

  /**
   * Method responsible for creating content when
   * the number of elements is less than 50.
   */
  private function generateContent($values) {
    foreach ($this->eckTypeStorage as $type) {
      $values[$type->id()] = array_filter($values[$type->id()]);
      if (!empty($values['kill']) && $values[$type->id()]) {
        $this->contentKill($values);
      }

      if (!empty($values[$type->id()])) {
        $start = time();
        for ($i = 1; $i <= $values['num']; $i++) {
          $this->develGenerateContentAddEntity($values, $type->id());
          if (function_exists('drush_log') && $i % drush_get_option('feedback', 1000) == 0) {
            $now = time();
            drush_log(dt('Completed @feedback entities (@rate entity/min)', array('@feedback' => drush_get_option('feedback', 1000), '@rate' => (drush_get_option('feedback', 1000) * 60) / ($now - $start))), 'ok');
            $start = $now;
          }
        }
      }
      // @TODO fix message for multiple types.
      $this->setMessage($this->formatPlural($values['num'], '1 entity created.', 'Finished creating @count entities'));
    }
  }

  /**
   * Method responsible for creating content when
   * the number of elements is greater than 50.
   */
  private function generateBatchContent($values) {
    // Setup the batch operations and save the variables.
    //$operations[] = array('devel_generate_operation', array($this, 'batchContentPreNode', $values));

    // Add the kill operation.
    if ($values['kill']) {
      $operations[] = array('devel_generate_operation', array($this, 'batchContentKill', $values));
    }

    // Add the operations to create the nodes.
    for ($num = 0; $num < $values['num']; $num ++) {
      $operations[] = array('devel_generate_operation', array($this, 'batchContentAddEntity', $values));
    }

    // Start the batch.
    $batch = array(
      'title' => $this->t('Generating Content'),
      'operations' => $operations,
      'finished' => 'devel_generate_batch_finished',
      'file' => drupal_get_path('module', 'devel_generate') . '/devel_generate.batch.inc',
    );
    batch_set($batch);
  }

  public function batchContentAddEntity($values, &$context) {
    foreach ($this->eckTypeStorage as $type) {
      if (!empty($values[$type->id()])) {
        $this->develGenerateContentAddEntity($values, $type->id());
      }
    }
    $context['results']['num']++;
  }

  public function batchContentKill($vars, &$context) {
    $this->contentKill($context['results']);
  }

  /**
   * Create one node. Used by both batch and non-batch code branches.
   */
  protected function develGenerateContentAddEntity(&$results, $entity_type) {
    foreach ($this->eckTypeStorage as $type) {
      if (empty($results[$entity_type])) {
        continue;
      }

      $bundle = array_rand(array_filter($results[$entity_type]));
      
      $entity = EckEntity::create([
        'type' => $bundle,
        'title' => $this->getRandom()->sentences(mt_rand(1, $results['title_length']), TRUE),
        //'uid' => $uid,
        'uid' => 1,
        'revision' => mt_rand(0, 1),
        'status' => TRUE,
        //'created' => REQUEST_TIME - mt_rand(0, $results['time_range']),
        'created' => REQUEST_TIME,
        'langcode' => $this->getLangcode($results),
      ]);

      // A flag to let hook_node_insert() implementations know that this is a
      // generated node.
      $entity->devel_generate = $results;

      // Populate all fields with sample values.
      $this->populateFields($entity);

      // See devel_generate_node_insert() for actions that happen before and after
      // this save.
      $entity->save();
    }
  }

  /**
   * Deletes all content of given entity types.
   *
   * @param array $values
   *   The input values from the settings form.
   */
  protected function contentKill($values) {
    foreach ($this->eckTypeStorage as $type) {
      $eids = \Drupal::service('entity.query')
        ->get($type->id())
        ->condition('type', $values[$type->id()], 'IN')
        ->execute();

      if (!empty($eids)) {
        $controller = \Drupal::entityManager()->getStorage($type->id());
        $entities = $controller->loadMultiple($eids);
        $controller->delete($entities);
        $this->setMessage($this->t('Deleted %count entities.', array('%count' => count($eids))));
      }
    }
  }
}
